import autoBind from 'auto-bind';


export const radToDeg = rad => rad * (180 / Math.PI);

export default class Rotator {
  constructor(element) {
    console.log('rotator module instantiated');
    autoBind(this);

    this.element = element;

    this.mouse = { x: 0, y: 0 };
    this.pivot = { x: 0, y: 0 };
    this.angle = 0;
    this.dragging = false;
    this.element.addEventListener('pointerdown', this.mousedown);
    this.element.addEventListener('touchmove', (e) => { e.preventDefault(); });
    window.addEventListener('pointerup', this.mouseup);
    window.addEventListener('mouseleave', this.mouseup);
    window.addEventListener('pointermove', this.mousemove);
  }

  mousedown() {
    this.dragging = true;
    const { left, top, width, height } = this.element.getBoundingClientRect();
    this.pivot.x = left + (width / 2);
    this.pivot.y = top + (height / 2);
  }

  mouseup() {
    this.dragging = false;
  }

  mousemove(e) {
    this.mouse.x = e.clientX;
    this.mouse.y = e.clientY;

    if (this.dragging) {
      this.angle = radToDeg(Math.atan2(this.mouse.x - this.pivot.x, this.pivot.y - this.mouse.y));
      this.element.style.transform = `translate(-50%, -50%) rotate(${this.angle}deg)`;
      this.element.dispatchEvent(new CustomEvent('rotated', { detail: this.angle }));
    }
  }

  setRotation(rotation) {
    this.element.style.transform = `translate(-50%, -50%) rotate(${rotation}deg)`;
  }
}

window.Rotator = Rotator;
