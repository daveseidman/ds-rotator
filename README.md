# DS Rotator

Small rotation widget that takes an html element and adds pointer event listeners
allowing the user to rotate the element based on the arc tangent angle from the
drag point's x/y coordinate. 
