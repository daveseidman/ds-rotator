const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.png/i,
        type: 'asset/inline',
      },
    ],
  },
  output: {
    filename: 'ds-rotator.js',
    path: path.resolve('dist'),
    assetModuleFilename: './assets/[name][ext][query]',
    library: 'ds-rotator',
    libraryExport: 'default',
    libraryTarget: 'umd',
  },
};
